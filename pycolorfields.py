import time

import signal
import pickle

from collections import deque

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout

# from PyQt5.QtGui import QImage
# from PyQt5.QtGui import QPen
from PyQt5.QtGui import QColor
from PyQt5.QtGui import QBrush
from PyQt5.QtGui import QPalette
from PyQt5.QtGui import QPainter

# from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt
from PyQt5.QtCore import QRect
from PyQt5.QtCore import QRunnable
from PyQt5.QtCore import QThreadPool
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QObject

import sacn
# from sys import exit

CURRENT_MS = lambda: int(round(time.time() * 1000))

def testColorMap():
    # print("Inside testcolormap")
    f = 255
    mymap = []
    # print("f is :", f)
    while f > 0:
        mymap.append(f)
        mymap.append(f)
        # print(mymap)
        f = f -1
        # print("F is now: ", f)
    return deque(mymap)

class userFields():
    def __init__(self):
        self.origin_x = 0
        self.origin_y = 0
        self.height = 64
        self.width = 64
        self.spacing_x = 32
        self.spacing_y = 32
        self.qty = 20
        self.colormap = testColorMap()
        # self.colormap = []

    def setColorMap(self, newmap):
        self.colormap = deque(newmap)

    def setSpacingY(self, i):
        self.spacing_y = i

    def setSpacingX(self, i):
        self.spacing_y = i
            
    def setWidth(self, i):
        self.width = i
    
    def setHeight(self, i):
        self.height = i


_FIELDS = userFields()

_RASTERW = 1920 #TODO: set dynamically
_RASTERH = 1080

def save_settings():
    """ Saves settings to a file """
    pickle.dump(_FIELDS, open("fields.userconf", "wb"))
    print("Settings saved")


def load_settings():
    """ Loads settings from a file """
    global _FIELDS
    try:
        _FIELDS = pickle.load(open("fields.userconf", "rb"))
        print("Loaded successfully")
    except:
        print("Error in loading file, check if it exists")
            

def receive_test():
    """ Tests receiving sACN data for 10s, prints to console """
    receiver = sacn.sACNreceiver()
    receiver.start()

    @receiver.listen_on('universe', universe=1)
    def callback(packet):
        print(packet.dmxData)
        print(CURRENT_MS())

    time.sleep(10)
    receiver.stop()

class ExitDialog(QDialog):
    """ Dialog box to confirm an exit. """
    def __init__(self):
        QDialog.__init__(self)

        dialogLayout = QVBoxLayout()
        btnLayout = QHBoxLayout()
        warningLabel = QLabel("Are you sure you want to quit?")
        btn_yes = QPushButton("Yes")
        btn_no = QPushButton("No")

        btn_no.clicked.connect(lambda: self.close())
        btn_yes.clicked.connect(lambda: exit())

        btnLayout.addWidget(btn_no)
        btnLayout.addWidget(btn_yes)
        dialogLayout.addWidget(warningLabel)
        dialogLayout.addLayout(btnLayout)
        
        self.setLayout(dialogLayout)
        self.setWindowTitle("Quit?")

        self.exec_()


class ColorRegion(QRect):
    """ TBD if this will be used? """
    def __init__(self):
        QRect.__init__(self)

class configWindow(QDialog):
    """ The window where all the magic happens """
    def __init__(self):
        QDialog.__init__(self)
        self.setWindowTitle("pycolorfields Config")

        configLayout = QFormLayout()

        self.testLine = QLineEdit()
        btn_submit = QPushButton("Submit")
        btn_submit.clicked.connect(lambda: self.submit())
        btn_quit = QPushButton("Quit")
        btn_quit.clicked.connect(lambda: ExitDialog())
        btn_load = QPushButton("Load Settings")
        btn_save = QPushButton("Save Settings")
        btn_load.clicked.connect(lambda: load_settings())
        btn_save.clicked.connect(lambda: save_settings())

        configLayout.addRow(QLabel("Test box: "), self.testLine)
        configLayout.addRow(btn_submit)
        configLayout.addRow(btn_load)
        configLayout.addRow(btn_save)
        configLayout.addRow(btn_quit)

        self.setLayout(configLayout)


    def submit(self):
        print(self.testLine.text())
        self.close()

class receive_signals(QObject):
    newDMX = pyqtSignal(object)

class receive_loop(QRunnable):
    """ Event Loop to receive DMX data over sACN """
    # newDMX = pyqtSignal(object)
    def __init__(self):
        QRunnable.__init__(self)
        self.signal = receive_signals()

    @pyqtSlot()
    def run(self):
        receiver = sacn.sACNreceiver()
        receiver.start()

        @receiver.listen_on('universe', universe=1)
        def callback(packet):
            print("Received new DMX Data test1")
            self.signal.newDMX.emit(packet.dmxData)
            # print("Received new DMX data")
            # _FIELDS.setColorMap(packet.dmxData)
            # self.newDMX.emit(packet.dmxData)
            # print(packet.dmxData)
            # print(CURRENT_MS())


class mainWindow(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        self.initUI()
        self.threadpool = QThreadPool()
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())

        self.start_listening()



    def initUI(self):
        self.setPalette(QPalette(Qt.black))
        self.setWindowTitle("pycolorfields")
        self.setCursor(Qt.BlankCursor)
            
    def start_listening(self):
        sacnLoop = receive_loop()
        sacnLoop.signal.newDMX.connect(self.got_new_DMX)
        self.threadpool.start(sacnLoop)

    def got_new_DMX(self, newDMX):
        _FIELDS.setColorMap(newDMX)
        self.repaint()

    def configPopup(self):
        newPopup = configWindow()
        newPopup.exec_()

    
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_G:
            self.configPopup()
        if event.key() == Qt.Key_Q:
            _FIELDS.setHeight(_FIELDS.height - 32)
        if event.key() == Qt.Key_A:
            _FIELDS.setHeight(_FIELDS.height + 32)
        if event.key() == Qt.Key_W:
            _FIELDS.setWidth(_FIELDS.width -32)
        if event.key() == Qt.Key_S:
            _FIELDS.setWidth(_FIELDS.width + 32)
        # if event.key() == Qt.Key_Q:
        #     ExitDialog()

        # if event.key() == Qt.Key_D:
        #     self.paintEvent(event)
    
    # TODO: handle the DMX parsing elsewhere
    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        painter.setPen(Qt.NoPen)
        print("FUCK")
        self.draw_shapes(painter)
        painter.end()

    def draw_shapes(self, painter):

        brush = QBrush()
        # print("Just made a brush")
        brush.setStyle(Qt.SolidPattern)
        # print("Just set a brush style")
        # painter.setBrush(brush)
        # print("just set the painter to be brush")
        f = _FIELDS.qty # To count down from 512, or whatever else it is
        # print("f, taken from the fields qty, is: ", f)
        w = _FIELDS.origin_x
        # print("w is taken from the fields x origin ", w)
        h = _FIELDS.origin_y
        # print("h is taken from the fields y origin ", h)

        # Where we're working in the array of the color map 
        index = 0 
        # print("set the 'index' for upcoming array to 0 for starters")
        boxes_drawn = 0
        while f > 0:
            # See if shit fits inside the raster
            if (w + _FIELDS.width) > _RASTERW:
                w = 0
                h = h + _FIELDS.height + _FIELDS.spacing_y
            if (h+_FIELDS.height) > _RASTERH:
                print("failure - too much shit for one raster")
                break

            # Assign values to r, g, and b from the DMX stream, aka
            # the "COLOR MAP" because that's fuckin what I'm calling it
            try:
                r = _FIELDS.colormap.popleft()
                g = _FIELDS.colormap.popleft()
                b = _FIELDS.colormap.popleft()
                index = index + 3
            except IndexError:
                print("INDEX ERROR: ")
                break
            except:
                print("UNCAUGHT EXCEPTION")
                pass


            if r > 255 or g > 255 or b > 255:
                print(_FIELDS.colormap)


            # print(r,g,b)
            brush.setColor(QColor(r, g, b))
            rect = QRect(w, h, _FIELDS.width, _FIELDS.height)
            painter.setBrush(brush)
            painter.drawRect(rect)
            w = w + _FIELDS.spacing_x + _FIELDS.width
            # print("drew these colors: ", r, g, b)



        

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    # receive_test()

    app = QApplication([])
    # This here prevents the "?" button from appearing on any windows!
    app.setAttribute(Qt.AA_DisableWindowContextHelpButton)
    windup = mainWindow()
    windup.showFullScreen()
    app.exec_()